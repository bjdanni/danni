def search(my_list, num):
    """
    :param my_list:
    :param num:
    :return:
    """
    low = 0
    high = len(my_list) - 1

    while low <= high:
        mid = int((low + high) / 2)
        guess = my_list[mid]

        if guess == num:
            return mid
        elif guess > num:
            high = mid - 1
        else:
            low = mid + 1
    return None


if __name__ == '__main__':
    search([1, 2, 3, 4, 5, 6, 7], 3)